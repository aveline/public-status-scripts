<?php
	define('IN_AVELINES_SCRIPTS', true);
	define('STARTUP_TS', time());

	require 'include/common.php';
	require 'include/access.php';

	if(strtoupper($_SERVER['REQUEST_METHOD']) !== 'POST') {
		exit('POST ONLY!');
	}

	$secret = $_POST['secret'];
	$name = $_POST['name'];
	$data = array(
		'net_up'	=> $_POST['net_send'],
		'net_down'	=> $_POST['net_recv'],
		'uptime'	=> $_POST['uptime'],
		'loadavg'	=> $_POST['loadavg'],
		'cpu'		=> $_POST['cpu'],
	);

	$precheck = write_data_precheck($name, $secret, $data);

	if($precheck) {
		write_data($name, $secret, $data);
		echo 'DONE';
	} else {
		exit('ERROR');
	}