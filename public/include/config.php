<?php
	if(!defined('IN_AVELINES_SCRIPTS')) {
		exit('Yamiedie ...');
	}

	$yaml_suffix = '.yaml.php';

	$config = Spyc::YAMLLoad('data/config' . $yaml_suffix);

	extract($config);
