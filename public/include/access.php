<?php
	if(!defined('IN_AVELINES_SCRIPTS')) {
		exit('Yamiedie ...');
	}

	function remote_ip() {
		return $_SERVER['REMOTE_ADDR'];
	}

	function ip_compare($a, $b) {
		// TODO
		return $a == $b;
	}

	function data_path($name) {
		return 'data/report-' . strtolower(MD5($name)) . '.php';
	}
	
	function write_data_precheck($name, $secret, $data) {
		global $nodes;

		if(!isset($nodes[$name])) {
			exit('PRECHECK ERROR: NO NODE `' .  $name . '`');
		}

		if(isset($nodes[$name]['limit_ipaddr']) && !ip_compare($nodes[$name]['limit_ipaddr'], remote_ip())) {
			exit('PRECHECK ERROR: INVALID IP ADDRESS');
		}

		if($secret != $nodes[$name]['secret']) {
			exit('PRECHECK ERROR: INVALID SECRET');
		}

		return true;
	}

	function write_data($name, $secret, $data) {
		$write_data     = '<' . '?' . 'php exit; ' . '?' . '>' . "\n";
		$write_data    .= serialize($data); 
		return file_put_contents(data_path($name), $write_data);
	}

	function read_seriazlied($path) {
		$data = file($path);
		$data = unserialize($data[1]);
		$data['lastupdate'] = filemtime($path);

		return $data;
	}

	function read_data($name) {
		return file_exists(data_path($name)) ? read_seriazlied(data_path($name)) : false;
	}