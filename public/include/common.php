<?php
	if(!defined('IN_AVELINES_SCRIPTS')) {
		exit('Yamiedie ...');
	}

	// Load YAML Parser
	require_once 'spyc.php';

	// Load Config
	require_once 'config.php';

	// Load i18n files
	switch($language) {
		case 'zh-CN':
			require 'i18n/zh-CN.php';
			break;
		default:
			break;
	}


	function tpl($page = 'none') {
		// TODO: Rewrite this function
		return $page == 'status' ? 'templates/status.php' : 'templates/404.php';
	}

	function lang($str) {
		return isset($GLOBALS['lang'][$str]) ? $GLOBALS['lang'][$str] : $str;
	}
