<!DOCTYPE html>
	<html lang="en">
	<head>
	<meta charset="utf-8" />
	<title><?php echo $site_title; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<!-- Le styles -->
	<link href="css/bootstrap.css" rel="stylesheet">
	<style type="text/css">
		body {
			padding-top: 60px;
		}
		.table-center th,
		.table-center td {
			text-align: center;
		}
	</style>
	<link href="css/bootstrap-responsive.css" rel="stylesheet">

	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>

<body>

	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="#"><?php echo $site_title; ?></a>
				<div class="nav-collapse">
				<ul class="nav">
				<li class="active"><a href="#">Home</a></li>
				</ul>
				</div><!--/.nav-collapse -->
				</div>
			</div>
		</div>

		<div class="container">

		<ul class="nav nav-tabs">
			<?php foreach(array_keys($groups) as $i=>$group) { ?>
			<li<?php if($i==0) { echo ' class="active"'; } ?>><a href="#g<?php echo $i;?>" data-toggle="tab"><?php echo $group; ?></a></li>
			<?php } ?>
		</ul>

		<div class="tab-content">

		<?php
			$groupI = 0;
			foreach($groups as $groupId=>$group) {
				$groupI++;
				$_nodes = array();

				foreach($group['nodes'] as $node) {
					if(isset($nodes[$node])) {
						$_nodes[] = $nodes[$node];
					}
				}
		?>
			<div class="tab-pane active" id="<?php echo $groupI; ?>">
				<table class="table table-striped table-bordered table-center">
					<thead>
					<tr>
					<th><?php echo lang('Server'); ?></th>
					<th><?php echo lang('Last Update'); ?></th>
					<th><?php echo lang('Uptime'); ?></th>
					<th><?php echo lang('Average Load'); ?></th>
					<th><?php echo lang('CPU Usage'); ?></th>
					<th><?php echo lang('Bandwidth Out/In'); ?></th>
					</tr>
					</thead>

					<tbody>
						<?php foreach ($_nodes as $nodeName=>$node) { ?>
						<tr>
							<td><?php echo $node['name']; ?></td>
							<?php
								$nodeData = read_data($nodeName);
								if(!$nodeData) {
									echo '							<td>Never</td>';
									echo str_repeat('<td>No Data</td>', 4);
								} else {
									echo '							<td>' . sprintf(lang("%u seconds ago"), (time() - $nodeData['lastupdate'])) .'</td>';

							?>
							<td><?php echo $nodeData['uptime']; ?></td>
							<td><?php echo $nodeData['loadavg']; ?></td>
							<td><?php echo $nodeData['cpu']; ?></td>
							<td><?php echo $nodeData['net_up']; ?> / <?php echo $nodeData['net_down']; ?></td>
							<?php
								}
							?>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		<?php
			}
		?>
		</div>

		<hr>

		<footer class="footer">
		<p class="pull-right"><a href="#">Back to top</a></p>
		<p>Written By <a href="http://vii.im/">Aveline Swan</a></p>
		</footer>
	</div> <!-- /container -->

	<script src="js/bootstrap.min.js"></script>
</body>
</html>