<?php
	if(!defined('IN_AVELINES_SCRIPTS')) {
		exit('Yamiedie ...');
	}

	$GLOBALS['lang'] = array(
		'Server'					=> '服务器',
		'Last Update'				=> '最后通讯',
		'Uptime'					=> '开机时间',
		'Average Load'				=> '平均负载',
		'CPU Usage'					=> 'CPU使用率',
		'Bandwidth Out/In'			=> '带宽 上传/下载',

		'%u seconds ago'			=> '%u 秒前',
	);