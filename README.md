# Installion
*Chinese*

## 服务端/监控页面
1.下载&上传 **public** 文件夹

2.修改 **data/config.yaml.php** 

这个文件是YAML的。。。应该都会吧。。。嗯会我就不说了。。。大家自己看


## 客户端
 *目前仅在RHEL系系统上测试通过*
 
1.下载 **scripts/stat.sh **
 
	mkdir -p /usr/local/scripts
	cd /usr/local/scripts
	wget --no-check-certificate https://bitbucket.org/aveline/public-status-scripts/raw/4fd573b496d5/scripts/stat.sh
	chmod +x stat.sh

2.编辑 **scripts/stat.sh **
 用任意编辑器打开即可，修改以下部分
 	secret='密钥'
	node_name='节点名称'
	url='保存地址'
 
3.添加到crontab中，如

	*/5 * * * * /usr/local/scripts/stat.sh >> /usr/local/scripts/stat.log >&1



# Copyright & License

This library is licenced under the BSD license.

Copyright (C) 2011, Aveline Swan <i@vii.im>.

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

* Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
